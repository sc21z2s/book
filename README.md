---
title: COMP2421 Numerical computation
---
# Module information

**All dates below are subject to change.**

Module leader
:   Dr Thomas Ranner (Tom)

Email
:   T.Ranner@leeds.ac.uk

Teams group
:   [22/23(1) COMP2421 Numerical Computation (32879)](https://teams.microsoft.com/l/team/19%3aMD-x6E4QiyZ3S1zNFHomIWxtG3UJiRimjR_RzgapA7g1%40thread.tacv2/conversations?groupId=54035a7f-8376-45f0-b7b8-d4fac9d5ab54&tenantId=bdeaeda8-c81d-45ce-863e-5232a535b7cb)

## Course contents

-   **Vectors and matrices**: introduction and justification; vector and matrix operations; identity matrix; inverse of a matrix.

-   **Approximation and errors**: modelling and mathematical modelling; discrete and continuous models; floating point and rounding errors; balancing accuracy and efficiency.

-   **Static systems**: iterative methods for solving nonlinear scalar equations; methods for solving linear systems of equations; systems without unique solutions.

-   **Evolving systems**: derivatives and rates of change; initial value problems; stability and convergence of computer models.

## Module components

### Lectures

2 hours per week

- Thursdays at 12:00
- Fridays at 11:00

### Tutorials

1 hour per week face to face (see timetable)

Opportunity to get **feedback** on learning

### Homework and coursework exercise

These will be provided *weekly* throughout the semester.

Courseworks are designed to give practice and reinforce the lectures. Coursework material is examinable.

## Assessment

### Examination

Computer exam in January. 2 hour exam with some multiple choice, short and longer answer questions. The exam is worth 60% of the credit for the module.

See [Lecture 20](./lec/lec20.md) for details.

*Edit: exam type changed - please follow updated guidance*

### Summative coursework

There are two pieces of coursework for this module. Each is worth 20% of your module grade. Coursework will be submitted via Gradescope. The dates are tentative at this stage and are subject to change.

**Feedback** will be provided in tutorials and written group feedback.

<table class="colwidths-auto table">
	<caption>Summative coursework timetable</caption>
	<thead>
	<tr>
	<th class="head">Coursework</th>
	<th class="head">Date set</th>
	<th class="head">Submission deadline</th>
	<th class="head">Feedback date</th>
	</tr>
	</thead>
	<tr>
	<td>1</td>
	<td>17/10/22</td>
	<td>8/11/22</td>
	<td>22/11/22</td>
	</tr>
	<tr>
	<td>2</td>
	<td>28/11/22</td>
	<td>13/12/22</td>
	<td>early 23</td>
	</tr>
</table>

Late submission will only be allowed up to 1 week after the submission deadline to allow feedback to be returned in a reasonable time frame.

### Formative homework

There will be weekly homework assignment sheets. These are not for credit. Additional feedback will be given through the release of worked solutions and in tutorial sessions.

### How to succeed?

- Read lecture notes **before lectures** -- all available
- Turn up to lectures with **pen and paper**
- Attempt **worksheet** before tutorial
- Let tutorial leader know where you are having problems before the session
- Attend tutorial every week - ask questions

### How to get 100%?

**No one gets 100%**

- You should be able to apply all algorithms to get 40-50%.
- You should be able to apply modifications to algorithms to get 50-60%
- You should be able to explore algorithms to get 60-70%
- You should be able to understand why algorithms are good/bad to get 70%+
- You should be able to write research papers on this topic to get 90%+

## Syllabus

This is a rough breakdown of topics to be covered this semester. Please note that this is not entirely fixed and I cannot guarantee to follow this precise structure.

<table class="colwidths-auto table">
<caption>Lecture topics</caption>
<thead>
<tr class="row-odd"><th class="head"><p>Lecture</p></th>
<th class="head"><p>Topic</p></th>
</tr>
</thead>
<tbody>
<tr class="row-even"><td><p>1 (week 1)</p></td>
<td><p>Introduction</p></td>
</tr>
<tr class="row-odd"><td><p>2</p></td>
<td><p>Floating point number systems</p></td>
</tr>
<tr class="row-even"><td><p>3 (week 2)</p></td>
<td><p>Vectors and matrices</p></td>
</tr>
<tr class="row-odd"><td><p>4</p></td>
<td><p>Linear transformations</p></td>
</tr>
<tr class="row-even"><td><p>5 (week 3)</p></td>
<td><p>Solving triangular systems of linear equations</p></td>
</tr>
<tr class="row-odd"><td><p>6</p></td>
<td><p>Gaussian elimination</p></td>
</tr>
<tr class="row-even"><td><p>7 (week 4)</p></td>
<td><p>LU factorisation</p></td>
</tr>
<tr class="row-odd"><td><p>8</p></td>
<td><p>Indirect methods (Jacobi, Gauss-Seidel)</p></td>
</tr>
<tr class="row-even"><td><p></p></td>
<td><p><em>reading week</em></p></td>
</tr>
<tr class="row-odd"><td><p>9 (week 6)</p></td>
<td><p>Sparse systems of linear equations, norms</p></td>
</tr>
<tr class="row-even"><td><p>10</p></td>
<td><p>Pivoting</p></td>
</tr>
<tr class="row-odd"><td><p>11 (week 7)</p></td>
<td><p>Derivatives and rates of change</p></td>
</tr>
<tr class="row-even"><td><p>12</p></td>
<td><p>Euler’s method</p></td>
</tr>
<tr class="row-odd"><td><p>13 (week 8)</p></td>
<td><p>Euler &amp; midpoint methods</p></td>
</tr>
<tr class="row-even"><td><p>14</p></td>
<td><p>Runge-Kutte integration</p></td>
</tr>
<tr class="row-odd"><td><p>15 (week 9)</p></td>
<td><p>Bisection, Newton’s method</p></td>
</tr>
<tr class="row-even"><td><p>16</p></td>
<td><p>Newton’s method</p></td>
</tr>
<tr class="row-odd"><td><p>17 (week 10)</p></td>
<td><p>The secant method</p></td>
</tr>
<tr class="row-even"><td><p>18</p></td>
<td><p>Hybrid methods</p></td>
</tr>
<tr class="row-odd"><td><p>19 (week 11)</p></td>
<td><p>Special topics</p></td>
</tr>
<tr class="row-even"><td><p>20</p></td>
<td><p>Review</p></td>
</tr>
</tbody>
</table>

### Tutorials plan

Weekly tutorials should will support you in your learning. See your timetable for when and where you should attend.

<table class="colwidths-auto table">
<caption>Tutorials plan</caption>
<thead>
<tr class="row-odd"><th class="head"><p>Week</p></th>
<th class="head"><p>Topic</p></th>
</tr>
</thead>
<tbody>
<tr class="row-even"><td><p>1</p></td>
<td><p>Maths preliminary</p></td>
</tr>
<tr class="row-odd"><td><p>2</p></td>
<td><p>Introduction to python</p></td>
</tr>
<tr class="row-even"><td><p>3</p></td>
<td><p>Floating point number systems</p></td>
</tr>
<tr class="row-odd"><td><p>4</p></td>
<td><p>Triangular systems and Gaussian elimination</p></td>
</tr>
<tr class="row-even"><td><p>-</p></td>
<td><p><em>reading week</em></p></td>
</tr>
<tr class="row-odd"><td><p>6</p></td>
<td><p>LU Factorisation and iterative methods</p></td>
</tr>
<tr class="row-even"><td><p>7</p></td>
<td><p>Sparse systems/pivoting</p></td>
</tr>
<tr class="row-odd"><td><p>8</p></td>
<td><p>Derivatives and Euler’s method</p></td>
</tr>
<tr class="row-even"><td><p>9</p></td>
<td><p>Other time stepping</p></td>
</tr>
<tr class="row-odd"><td><p>10</p></td>
<td><p>Bisection and Newton’s method</p></td>
</tr>
<tr class="row-even"><td><p>11</p></td>
<td><p>Other root finding</p></td>
</tr>
</tbody>
</table>

## Contact

- Please contact me through MS Teams [22/23(1) COMP2421 Numerical Computation (32879)](https://teams.microsoft.com/l/team/19%3aMD-x6E4QiyZ3S1zNFHomIWxtG3UJiRimjR_RzgapA7g1%40thread.tacv2/conversations?groupId=54035a7f-8376-45f0-b7b8-d4fac9d5ab54&tenantId=bdeaeda8-c81d-45ce-863e-5232a535b7cb) (not email)
- Questions about content should go in the class team
- Responses in 24h (or out of office)
- No response out of working hours

## Reference materials

The programming for this module will be carried out using `python3`.

-   For general Python, refer to your first-year teaching materials.

-   For a refresher course in Python:

    - <https://developers.google.com/edu/python/?hl=en>
	- <https://learnxinyminutes.com/docs/python/>

-   For `numpy`, `scipy` etc:

    - <http://scipy-lectures.github.io>
    - <https://github.com/jrjohansson/scientific-python-lectures>

## Links

-   Module catalogue:

	- COMP2421 <https://webprod3.leeds.ac.uk/catalogue/dynmodules.asp?Y=202122&M=COMP-2421>\
    - XJCO2421 <https://webprod3.leeds.ac.uk/catalogue/dynmodules.asp?Y=202122&M=XJCO-2421>
