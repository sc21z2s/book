{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "33401089",
   "metadata": {},
   "source": [
    "# WS02: Floating point number systems"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "3a6be719",
   "metadata": {},
   "source": [
    "These exercises are indented to give you practice at using the material on numerical approximation and are intended to reinforce the material that was covered in lectures.\n",
    "\n",
    "Please attempt the worksheet before your tutorial. Support is available in your tutorial or in the Class Team."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "8c7431ea",
   "metadata": {},
   "source": [
    "*[Partial solutions](./ws02_implemented) are available to check your understanding. Please create [Issues](https://gitlab.com/comp2421-numerical-computation/book/-/issues) and [Merge requests](https://gitlab.com/comp2421-numerical-computation/book/-/merge_requests) with your solutions or share your answers for peer feedback in the class team [Worksheet solutions channel](https://teams.microsoft.com/l/channel/19%3a43e9bb00fad04d8d8c85399b694657f4%40thread.tacv2/Worksheet%2520solutions?groupId=54035a7f-8376-45f0-b7b8-d4fac9d5ab54&tenantId=bdeaeda8-c81d-45ce-863e-5232a535b7cb)*"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "38ce0afb",
   "metadata": {},
   "source": [
    "# Part a (pen and paper warm up)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "3bd57fb9",
   "metadata": {},
   "source": [
    "### 1. Number systems\n",
    "\n",
    "Consider the number system given by $(\\beta, t, L, U) = (10, 3, -3, 3)$ which gives\n",
    "\n",
    "$$\n",
    "x = \\pm .b_1 b_2 b_3 \\times 10^e \\text{ where } -3 \\le e \\le 3.\n",
    "$$\n",
    "\n",
    "-  How many numbers can be represented by this normalised system?\n",
    "\n",
    "-  What are the two largest positive numbers in this system?\n",
    "\n",
    "-  What are the two smallest positive numbers?\n",
    "\n",
    "-  What is the smallest possible difference between two numbers in this system?\n",
    "\n",
    "-  What is the smallest possible difference in this system, $x$ and $y$, for which $x < 100 < y$?"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "2900bb3f",
   "metadata": {},
   "source": [
    "### 2. Do floating point operations commute?\n",
    "\n",
    "-  Let\n",
    "\n",
    "    $$\n",
    "     x = .85 \\times 10^0, \\quad\n",
    "     y = .3 \\times 10^{-2}, \\quad\n",
    "     z = .6 \\times 10^{-2},\n",
    "    $$\n",
    "    \n",
    "    in the system $(\\beta, t, L, U) = (10, 2, -3, 3)$. Evaluate the following expression in this number system.\n",
    "    \n",
    "    $$\n",
    "    x+(y+y), \\quad\n",
    "    (x+y)+y, \\quad\n",
    "    x+(z+z), \\quad\n",
    "    (x+z) +z.\n",
    "    $$\n",
    "   \n",
    "    (Also note the benefits of adding the *smallest* terms first!)\n",
    "\n",
    " \n",
    "-  Given the number system $(\\beta, t, L, U) = (10, 3, -3, 3)$ and $x = .100\\times 10^3$, find nonzero numbers $y$ and $z$ from this system for which $fl(x+y) = x$ and $fl(x+z) > x$."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "121fbe45",
   "metadata": {},
   "source": [
    "# Part b (floating point numbers in python)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d6ddf971",
   "metadata": {},
   "source": [
    "### 3. Floating point types in `numpy`\n",
    "\n",
    "`numpy` has information about it's floating point types built in. Run this code block and interpret the answers."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "a48e088e",
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "\n",
    "print(help(np.finfo))\n",
    "\n",
    "for dtype in [float, np.double, np.single, np.half]:\n",
    "    print(dtype.__name__, np.finfo(dtype))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "39a89242",
   "metadata": {},
   "source": [
    "### 4. Equality of floating point representations\n",
    "\n",
    "When working with floating point numbers you cannot simply test for equality (see also [this stackoverflow question](https://stackoverflow.com/questions/4915462/how-should-i-do-floating-point-comparison/4915891#4915891)).\n",
    "\n",
    "We want to test if we have computed the square root of two accurately."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "661f7950",
   "metadata": {},
   "outputs": [],
   "source": [
    "a = np.sqrt(2.0)\n",
    "print(f\"a={a}, type(a)={type(a)}\")\n",
    "\n",
    "b = a*a\n",
    "print(f\"b={b}\")\n",
    "\n",
    "print(b == 2.0)\n",
    "print(b-2.0)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "c1f1cf76",
   "metadata": {},
   "source": [
    "Disaster! One option is to use [`numpy.isclose`](https://numpy.org/doc/stable/reference/generated/numpy.isclose.html), which roughly equivalent to this code:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "6c68b2ad",
   "metadata": {},
   "outputs": [],
   "source": [
    "def my_isclose(x, y, tol=1.0e-9):\n",
    "    return abs(x - y) < tol\n",
    "\n",
    "print(my_isclose(a*a, b))\n",
    "print(np.isclose(a*a, b))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a8cc3d65",
   "metadata": {},
   "source": [
    "### 5. What's the best way to write a function\n",
    "\n",
    "Show that two functions\n",
    "\n",
    "   $$\n",
    "   f(x) = x ( \\sqrt{x+1} - \\sqrt{x}) \\qquad \\mbox{ and } \\qquad\n",
    "   g(x) = \\frac{x}{\\sqrt{x+1} + \\sqrt{x}},\n",
    "   $$\n",
    "   are equivalent.\n",
    "   Evaluate $f(500)$ and $g(500)$ using double precision (`np.float64`), single precision (`np.float32`) and half precision (`np.float16`). You should use `numpy.sqrt` to compute square roots.\n",
    "   Explain why these answers are different and comment on which is the more accurate and why."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "4e1feb2c",
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "\n",
    "def f(x):\n",
    "    # TODO implement me\n",
    "\n",
    "def g(x):\n",
    "    # TODO implement me"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "88e18df4",
   "metadata": {},
   "outputs": [],
   "source": [
    "# test f(x) and g(x)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "81bc5502",
   "metadata": {},
   "source": [
    "### 6. Ordering of floating point calculations in practice\n",
    "\n",
    "For this question you are required to write a function `pySquared`. This function should make use of the formula\n",
    "\n",
    "$$\n",
    "\\frac{1}{6} \\pi^2 = \\sum_{k=1}^\\infty \\frac{1}{k^2}\n",
    "$$\n",
    "\n",
    "to estimate the value of $\\pi^2$ by computing the partial sum\n",
    "\n",
    "$$\n",
    "\\pi^2 \\approx 6 \\sum_{k=1}^n \\frac{1}{k^2}\n",
    "$$\n",
    "\n",
    "for large values of $n$."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a3c44e56",
   "metadata": {},
   "source": [
    "a.  Using double precision and the predefined constant `pi`, produce a table that gives the absolute error in the approximation to $\\pi^2$ that you obtain using your function `piSquared` with $n = \\{10^6, 10^7, 10^8, 10^9\\}$. What is the difference between your answers when $n=10^8$ and $10^9$? Can you explain what is happening?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "17eddc64",
   "metadata": {},
   "outputs": [],
   "source": [
    "def piSquared(n):\n",
    "    # TODO implement me\n",
    "\n",
    "# TODO make table"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "29bcaf7c",
   "metadata": {},
   "source": [
    "b.  Now modify the function `piSquared` to compute the same sum of the $n$ terms but to add them up in the opposite order (i.e., start with the smallest terms first - which correspond to the largest values of $k$ first). Call your new script `piSquared_v2`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "5809baaf",
   "metadata": {},
   "outputs": [],
   "source": [
    "def piSquared_v2(n):\n",
    "    # TODO implement me"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "c181c389",
   "metadata": {},
   "source": [
    "Produce a table that gives the absolute error in the approximation to $\\pi^2$ that you obtain using your modified `piSquared_v2` with $n = \\{10^6, 10^7, 10^8, 10^9\\}$. What difference do you see when using the modified function? What is the explanation for this function being superior to the original version?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "6b256e04",
   "metadata": {},
   "outputs": [],
   "source": [
    "# TODO make table"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "fcab0462",
   "metadata": {},
   "source": [
    "## Part c: Extension"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "0603306b",
   "metadata": {},
   "source": [
    "### 7. Check the largest number\n",
    "\n",
    "The IEEE single precision standard is $(\\beta, t, L, U)=(2, 23, -127, 128)$, which is available via numpy.single. First, use the following Python command to print the largest positive number in this system; then, write a python code (or use a calculator) to compute the largest positive number and check whether it is the same as the output using the Python command."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "fb03845c",
   "metadata": {},
   "outputs": [],
   "source": [
    "print(np.finfo(np.float32))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "09e16333",
   "metadata": {},
   "source": [
    "For the largest positive number, $0.11..1 \\times 2^{128}$, you may need to think what does $0.11..1$ mean for a binary (base is 2) number system.We are used to decimals (base is 10) in our everyday life, for example, $0.72 = 7\\times 10^{-1} + 2\\times 10^{-2}$. However, we only have 0 and 1 in a binary number system...\n",
    "Actually, we can convert a binary to decimal as follows:\n",
    "$(0.101)_{t=2} = (1 \\times 2^{-1} + 0 \\times 2^{-2}+ 1 \\times 2^{-3})_{t=10} = (1/2 + 0 + 1/8 )_{t=10}=(0.625)_{t=10}$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "684fbd74",
   "metadata": {},
   "outputs": [],
   "source": [
    "def largest(b,t,l,u):\n",
    "    y = np.float64(0)\n",
    "    b = np.float64(b)\n",
    "    t = int(t)\n",
    "    l = np.float64(l) \n",
    "    u = np.float64(u)     \n",
    "\"\"\"\n",
    "TODO: compute y=0.11..1 x 2^u\n",
    "\"\"\""
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "5f1db682",
   "metadata": {},
   "outputs": [],
   "source": [
    "print(f\"The largest number is {largest(2,24,-126,128)}.\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "0c89322f",
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.15"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
