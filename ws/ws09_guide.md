KEY OBJECTIVES:

- Students should be able to find solutions to scalar nonlinear problems using secant method
- Students should understand Newton's method is not robust

Worksheet:
https://comp2421-numerical-computation.gitlab.io/book/ws/ws09.html
Partial solutions:
https://comp2421-numerical-computation.gitlab.io/book/ws/ws09_implemented.html

Rough plan:

1. Ask how everyone is getting on with the course? Any problems?

Part a tutorial:

2. Work through the part a question. Encourage students to volunteer solutions
3. Goto part b if time

Part b tutorial:

2. Work through the part b implementations. Encourage students to volunteer solutions
3. Notice how many iterations are required
4. Goto part c if time

Part c tutorial:

2. Work through part c implementations. Encourage students to volunteer solutions

I'll drop print outs (worksheet, solutions, this guide) on either Friday or early Monday.
