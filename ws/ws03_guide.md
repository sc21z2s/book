KEY OBJECTIVES:

- Students should be able to perform Gaussian elimination and backwards substitution to solve systems of linear equations
- (advanced aim) Understand how Gaussian elimination scales and when it breaks

Worksheet:
https://comp2421-numerical-computation.gitlab.io/book/ws/ws03.html
Partial solutions:
https://comp2421-numerical-computation.gitlab.io/book/ws/ws03_implemented.html

Rough plan: Try Balance part a, b, c of ws02

1. Ask how everyone is getting on with the course? Any problems?
2. Get students to do some computations from part a (Q3) on the board
	- if students are reluctant to do, say that you can write but they should tell you what to write
3. I don't really have strong suggestions of how you should proceed with parts b and c. Some options for you to consider:

	- Get students to come up and implement methods
	- Write in provided solutions in the sessions
	- Hide implementations then talk through Q5 onwards

Print outs (worksheet, solutions, this guide) should be by my office from around 10am
