KEY OBJECTIVES:

- Make plots in `matplotlib`
- Students should be able to understand the derivative as rate of change of a quantity and compute derivatives using finite differences/Euler's method
- Advanced aim - deeper understanding of derivative geometrically

Worksheet:
https://comp2421-numerical-computation.gitlab.io/book/ws/ws06.html
Partial solutions:
https://comp2421-numerical-computation.gitlab.io/book/ws/ws06_implemented.html

Rough plan:

1. Ask how everyone is getting on with the course? Any problems?

Part a tutorial:

2. Work through the part a question. Encourage students to volunteer solutions

    (If time)...
3. Draw some pictures of what's going on by hand/on the board.
4. Make that plot in `matplotlib` ensuring that you add labels to the axes (code in Q6).

Part b tutorial:

2. Work through the part b implementations. Encourage students to volunteer solutions
3. Test solutions with part a answers. Show how to do this testing
4. Ensure students understand how to plot in `matplotlib` ensuring that you add labels to the axes (code in Q6).

Part c tutorial:

2. Work through part c implementations. Encourage students to volunteer solutions
3. Ensure students understand how to plot in `matplotlib` ensuring that you add labels to the axes (code in Q6).

I'll drop print outs (worksheet, solutions, this guide) on Friday.
